<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    //
    public function table()
    {
        # code...
        return view('page.table');
    }

    public function dataTable()
    {
        # code...
        return view('page.data-table');
    }
}
